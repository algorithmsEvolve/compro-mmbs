export const state = () => ({
    visitor_noted: false
})

export const getters = {
    visitor_noted(state) {
        return state.visitor_noted
    }
}

export const mutations = {
    setNoteVisitor(state, payload) {
        state.visitor_noted = payload
    }
}

export const actions = {
    async setNoteVisitorAction({ commit }, payload) {
        commit('setNoteVisitor', payload)
    }
}